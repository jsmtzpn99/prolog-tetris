:- use_module(library(clpfd)).
:- use_module(library(pce)).
:- dynamic(cell/2).
:- dynamic(fallingcellstart/1).
:- dynamic(picture/1).
:- dynamic(fallingblock/2).
:- dynamic(blockrot/1).
:- dynamic(fallingdir/1).
:- dynamic(fossilized/2).

init:- retractall(fallingstart(_)),
       retractall(picture(_)),
       retractall(fallingblock(_,_)),
       retractall(blockrot(_)),
       retractall(cell(_,_)),
       retractall(fallingdir(_)),
       retractall(fossilized(_,_)),
       assert(fallingdir(downright)),
       assert(blockrot(right)),
       initpce,
       new_block,
       draw_board,
       new(T, timer(1, message(@prolog, interpret_key, down))),
       picture(P),
       send(P, done_message, and(message(T, free), message(P, destroy))),
       send(T, start).

block([right, right, right], red).
block([right, right, upright], orange).
block([right, right, upleft], yellow).
block([right, right, downright], green).
block([right, right, downleft], blue).
block([right, upright, right], hot_pink).
block([right, downright, right], violet).
block([downright, right, upright], cyan).
block([downright, right, upleft], goldenrod).

new_block:-
    findall(Path-Color, block(Path, Color), PCs),
    length(PCs, Len),
    R is random(Len)+1,
    nth1(R, PCs, Path-Color),
    retractall(fallingblock(_,_)),
    assert(fallingblock(Path, Color)),
    retractall(fallingcellstart(_)),
    assert(fallingcellstart((4,4))).


rot(A, right, A):- !.
rot(A, R, A3):-
    Seq = [right, upright, upleft, left, downleft, downright | Seq],
    once((nextto(A, A2, Seq),
    nextto(R2, R, Seq))),
    rot(A2, R2, A3).

adjacent2((X1,Y),right,(X2,Y)):-
   X2 #= X1+1.
adjacent2((X1,Y1),downright,(X2,Y2)):-
    Y2 #= Y1+1,
    X2 #= X1+1,
    Y1 mod 2 =:= 1.
adjacent2((X,Y1),downright,(X,Y2)):-
    Y2 #= Y1+1,
    Y1 mod 2 =:= 0.
adjacent2((X,Y1),downleft,(X,Y2)):-
    Y2 #= Y1+1,
    Y1 mod 2 =:= 1.
adjacent2((X1,Y1),downleft,(X2,Y2)):-
    Y2 #= Y1+1,
    X2 #= X1-1,
    Y1 mod 2 =:= 0.
adjacent2((X1,Y),left,(X2,Y)):-
   adjacent2((X2,Y),right,(X1,Y)).
adjacent2(Cell1, upleft, Cell2):- adjacent2(Cell2, downright, Cell1).
adjacent2(Cell1, upright, Cell2):- adjacent2(Cell2, downleft, Cell1).
adjacent((X1,Y1),Dir,(X2,Y2)):-
   adjacent2((X1,Y1),Dir,(X2,Y2)),
   X1 #>= 1, X1 #=< 10,
   X2 #>= 1, X2 #=< 10,
   Y1 #>= 1, Y1 #=< 24,
   Y2 #>= 1, Y2 #=< 24.

fallingcells(_, [], Cell, [Cell]).
fallingcells(Rot, [Dir|T], Cell, [Cell|T2]):-
    rot(Dir, Rot, Dir2),
    adjacent(Cell, Dir2, NextCell),
    fallingcells(Rot, T, NextCell, T2).

paintblock:-
    fallingblock(Path, Color),
    fallingcellstart(Start),
    blockrot(Rot),
    fallingcells(Rot, Path, Start, Cells),
    forall((member(C, Cells), cell(C, Graphical)), send(Graphical, fill_pattern, colour(Color))).

draw_board:-
    forall(cell(_,C), send(C, fill_pattern, colour(black))),
    once(paintblock;true),
    forall(fossilized(Cell, Color), (cell(Cell, Graphical), send(Graphical, fill_pattern, colour(Color)))),
    picture(P), send(P, redraw, @default).

interpret_key2(down):-
    step.
interpret_key2(up):-
    blockrot(Rot),
    rot(Rot, upright, Rot2),
    fallingcellstart(Cell),
    manip_block(Cell, Rot2).
interpret_key2(left):-
    fallingdir(downleft),
    fallingcellstart((X,Y)),
    X2 is X-1,
    blockrot(Rot),
    manip_block((X2,Y), Rot).
interpret_key2(left):-
    \+fallingdir(downleft),
    retractall(fallingdir(_)), assert(fallingdir(downleft)), step.
interpret_key2(right):-
    fallingdir(downright),
    fallingcellstart((X,Y)),
    X2 is X+1,
    blockrot(Rot),
    manip_block((X2,Y), Rot).
interpret_key2(right):-
    \+fallingdir(downright),
    retractall(fallingdir(_)), assert(fallingdir(downright)), step.
interpret_key2(_):-
    draw_board.

interpret_key(K):-
    forall(interpret_key2(K), true).


manip_block(Cell, Rot):-
    fallingblock(Path,_),
    fallingcells(Rot, Path, Cell, Path2),
    forall(member(M, Path2), \+fossilized(M, _)),
    retractall(blockrot(_)),
    retractall(fallingcellstart(_)),
    assert(blockrot(Rot)),
    assert(fallingcellstart(Cell)).


fossilize:-
    fallingblock(Path, Color),
    fallingcellstart(Start),
    blockrot(Rot),
    fallingcells(Rot, Path, Start, Cells),
    forall(member(M, Cells), assert(fossilized(M, Color))),
    new_block,
    clear_lines.

step3:-
    fallingcellstart((X,Y)),
    fallingdir(FD),
    member(Dir, [FD, downright, downleft]),
    adjacent((X,Y), Dir, (X2,Y2)),
    blockrot(Rot),
    manip_block((X2,Y2), Rot), !.
step2:-
    once(step3; fossilize).
step:-
    forall(step2, true).

clear_lines:-
    forall((bagof(X, X^C^(fossilized((X,Y),C)), XYs),
        forall(between(1, 10, X), member(X, XYs)),
        retractall(fossilized((_,Y),_)),
        %retract(lines_cleared(Cleared)),
        %Cleared2 is Cleared+1,
        %assert(lines_cleared(Cleared2)),
        findall(X-Y2-Color, (fossilized((X,Y2),Color),Y2<Y,retract(fossilized((X,Y2),Color))), Above),
        forall(member(X-Y2-Color, Above), (Y3 is Y2+1, assert(fossilized((X,Y3),Color))))),
    true).


initpce:-
    new(P, picture('Hextris')),
    send(P, width, 640), send(P, height, 480),
    assert(picture(P)),
    forall(between(1, 24, Y),
       forall(between(1, 10, X),
           (new(C, circle(22)),
           X2 is X*20+(Y mod 2)*10,
           Y2 is Y*20*sqrt(3)/2,
           send(C, x, X2),
           send(C, y, Y2),
           send(C, colour, white),
           send(P, display, C),
           assert(cell((X,Y),C))))),
    send(P, colour, colour(black)),
    send(P, show, true),
    send(P, recogniser, new(K, key_binding)),
    send(K, function, cursor_left, message(@prolog, interpret_key, left)),
    send(K, function, cursor_right, message(@prolog, interpret_key, right)),
    send(K, function, cursor_up, message(@prolog, interpret_key, up)),
    send(K, function, cursor_down, message(@prolog, interpret_key, down)),
    send(K, function, 'SPC', message(@prolog, interpret_key, ' ')).


hextris:-
    init.


















